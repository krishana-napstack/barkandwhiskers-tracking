<?php
  /* Template Name: Buttermilk */
  $base_url = get_stylesheet_directory_uri() . '/templates/assets-buttermilk';
?>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!--========== Page Ttile ==========-->
    <title>Bark & Whiskers | Buttercup Bed</title>

    <!-- Google Font -->
    <link href='https://fonts.googleapis.com/css?family=Poppins:400,300,500,600,700' rel='stylesheet' type='text/css'>

    <!-- CSS Plugin Files -->
    <link href="<?php echo $base_url; ?>/css/lib/bootstrap.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/css/plugins/font-awesome.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/css/plugins/lineicons.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/vendors/magnific-popup/magnific-popup.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/vendors/bootstrap-select/css/bootstrap-select.min.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/vendors/owl-carousel/assets/owl.carousel.css" rel="stylesheet">
    <link href="<?php echo $base_url; ?>/css/plugins/animate.css" rel="stylesheet">

    <!-- Preloader -->
    <link href="<?php echo $base_url; ?>/css/plugins/preloader.css" rel="stylesheet">

    <!--========== Main Styles==========-->
    <link href="<?php echo $base_url; ?>/css/style.css" rel="stylesheet">

    <!-- Theme CSS (For Available Color Options, See Documentation) -->
    <link href="<?php echo $base_url; ?>/css/themes/violet-green.css" rel="stylesheet">

    <!--========== HTML5 shim and Respond.js (Required) ==========-->
    <!--[if lt IE 9]>
    <script src="js/lib/html5shiv.min.js"></script>
    <script src="js/lib/respond.min.js"></script>
    <![endif]-->

</head>

<body class="home" data-scroll-animation="true" data-exit-modal="true">

    <!--==========Preloader==========-->
    <div id="loading">
        <div id="loading-center">
            <div id="loading-center-absolute">
                <div class="object" id="object_four"></div>
                <div class="object" id="object_three"></div>
                <div class="object" id="object_two"></div>
                <div class="object" id="object_one"></div>
            </div>
        </div>
    </div>

    <!--==========Header==========-->
    <header class="row alt-bg" id="header">
        <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container">
                <!--========== Brand and toggle get grouped for better mobile display ==========-->
                <div class="navbar-header">
                    <a class="navbar-brand" href="<?php echo esc_url( home_url( '/' ) ); ?>"><img src="<?php echo $base_url; ?>/images/bw_logo5.png" class="white-logo" alt=""></a>
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                        <span class="sr-only">Toggle navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                </div>

                <!--========== Collect the nav links, forms, and other content for toggling ==========-->
                <div class="collapse navbar-collapse" id="main-navbar">
                    <a href="https://www.barkandwhiskers.com/product/bowsers-plush-buttercup/" class=" btn btn-warning pull-right hidden-sm hidden-xs">Buy Yours Now</a>
                    <ul class="nav navbar-nav navbar-right">
                        <li><a href="#product">Design</a></li>
                        <li><a href="#features">Features</a></li>
						<li><a href="#styles">Styles</a></li>
                        <li><a href="#reviews">Reviews</a></li>
						<li><a href="#faq">FAQ</a></li>

                    </ul>
                </div>
                <!--========== /.navbar-collapse ==========-->
            </div>
            <!--========== /.container-fluid ==========-->
        </nav>

        <div class="top-banner row m0 text-center">
            <div class="container">

                <div class="row">
                    <div class="col-md-7 split-header m-t-100 text-left">
                        <h2>Deep Love<br/>Deep Peace<br/>Deep Dreams</h2>
                <p>Your dog will feel safe and secure sleeping in a Buttercup Bed. Tall soft walls hug your dog gently while drifting into a deep slumber, recreating feelings of warmth and closeness only a mother can provide.</p>
                        <a href="https://www.barkandwhiskers.com/product/bowsers-plush-buttercup/" target="_blank" class="btn btn-indegogo btn-lg">Buy Buttercup For Your Dog</a>
                    </div>

                    <!-- Image Section -->
                    <div class="col-md-5"><br /><br /><br /><br />
                        <img src="<?php echo $base_url; ?>/images/main2.1.png" alt="Watch" class="img-responsive center-block m-b-50-sm" />
                    </div>
                </div>

            </div>
        </div>
    </header>

    <!--==========The Product==========-->
    <section class="row the-product" id="product">
        <div class="container">
            <div class="row section-header wow fadeInUp">
                <h2>Designed for Comfort</h2>
                <p>The Buttercup Bed is made to fill your dog's wants and needs when it comes to getting a great night's sleep.  A drawstring lets you convert the Buttercup from a snuggly bed into a comfy lounge pad.</p>
            </div>

            <div class="row apple-watch-note-feature text-center">
                <img src="<?php echo $base_url; ?>/images/grey.jpg" alt="">

                <!--==========Feature Noted top right==========-->
                <div class="feature-note right top wow fadeInRight">
                    <div class="indicator">
                        <div class="plus-icon">
                            <span class="plus">+</span>
                        </div>
                    </div>
                    <div class="feature-name">
                        Warm Material
                    </div>
                </div>
                <!--==========Feature Noted top left==========-->
                <div class="feature-note left top wow fadeInLeft">
                    <div class="indicator">
                        <div class="plus-icon">
                            <span class="plus">+</span>
                        </div>
                    </div>
                    <div class="feature-name">
                        soft lip edge
                    </div>
                </div>
                <!--==========Feature Noted bottom right==========-->
                <div class="feature-note right bottom wow fadeInUp">
                    <div class="indicator">
                        <div class="plus-icon">
                            <span class="plus">+</span>
                        </div>
                    </div>
                    <div class="feature-name">
                        Tough & Clean
                    </div>
                </div>
                <!--==========Feature Noted bottom left==========-->
                <div class="feature-note left bottom wow fadeInLeft">
                    <div class="indicator">
                        <div class="plus-icon">
                            <span class="plus">+</span>
                        </div>
                    </div>
                    <div class="feature-name">
                        Easy Use Drawstring
                    </div>
                </div>
            </div>
        </div>
    </section>





    <!--==========Left Right Content==========-->
    <section class="row left-right-contents" id="features">
        <div class="container">
		<div class="row section-header wow fadeInUp">
                <h2>Why Dogs Love Buttercup</h2>
                <p>Dogs of all personalities choose to sleep in their Buttercup over other options. We don't speak dog, so we've only included the reasons they make obvious when you look at their cute little faces below:</p>
            </div>
            <div class="row ">
                <div class="col-sm-12 col-md-4 col-md-push-4 text-center wow fadeIn">
                    <img src="<?php echo $base_url; ?>/images/mid.jpg" alt="">
                </div>
                <div class="col-md-4 col-sm-6 col-md-pull-4 left-content">
                    <div class="media wow fadeInUp">
                        <div class="media-left">
                            <span><i class="li_heart"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>On-Demand Snuggles</h4>
                            <p>Ever worry if your dog gets enough attention at home while you're away? Buttercup's got you covered.</p>
                        </div>
                    </div>
                    <div class="media wow fadeInUp" data-wow-delay="0.3s">
                        <div class="media-left">
                            <span><i class="li_megaphone"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>Always Quiet At Night</h4>
                            <p>Buttercup is made of soft materials and fabrics. If your dog is awake at night you won't get woken up while they get comfy.</p>
                        </div>
                    </div>
                    <div class="media wow fadeInUp" data-wow-delay="0.6s">
                        <div class="media-left">
                            <span><i class="li_banknote"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>Best Bang For Your Buck</h4>
                            <p>Cheap dog beds feel like cardboard while fancy models cost $150+. Give your dog the most comfortable night's sleep without the sticker-shock.</p>
                        </div>
                    </div>
                </div>
                <div class="col-md-4 col-sm-6 right-content">
                    <div class="media wow fadeInUp">
                        <div class="media-left">
                            <span><i class="li_like"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>High Quality Fabrics</h4>
                            <p>Buttercups are made from high-quality microfiber that keeps your dog warm but not too hot. Upgrades also available.</p>
                        </div>
                    </div>
                    <div class="media wow fadeInUp" data-wow-delay="0.3s">
                        <div class="media-left">
                            <span><i class="li_bulb"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>Keep Nervous Dogs Calm</h4>
                            <p>Some dogs can't settle down until they feel safe. Watch as Buttercup holds your dog softly and their panic melts away.</p>
                        </div>
                    </div>
                    <div class="media wow fadeInUp" data-wow-delay="0.6s">
                        <div class="media-left">
                            <span><i class="li_clock"></i></span>
                        </div>
                        <div class="media-body">
                            <h4>Easy When Traveling</h4>
                            <p>Big and bulky dog beds are a nightmare when traveling. Buttercup is flexible and fits into all luggage. It's even light enough to take on a camping trip!</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <!--==========Our Collection==========-->
    <section class="row our-collection" id="styles">
        <div class="container">
            <div class="row section-header wow fadeInUp">
                <h2>Choose From Over 30 Styles</h2>
                <p>You'll find a perfect match for both your dog's personality <i>and</i> your home's decor. Here are just a few of our many designs:</p>
            </div>
            <div class="row collections">
                <!--==========Collection Items==========-->
                <div class="col-sm-6 col-md-3 item wow fadeIn">
                    <div class="row m0 featured-img">
                        <img src="<?php echo $base_url; ?>/images/studio.jpg" alt="">
                    </div>
                    <h4 class="title">Studio</h4>



                </div>
                <!--==========Collection Items==========-->
                <div class="col-sm-6 col-md-3 item wow fadeIn" data-wow-delay="0.5s">
                    <div class="row m0 featured-img">
                        <img src="<?php echo $base_url; ?>/images/camelot.jpg" alt="">
                    </div>
                    <h4 class="title">Camelot</h4>



                </div>
                <!--==========Collection Items==========-->
                <div class="col-sm-6 col-md-3 item wow fadeIn" data-wow-delay="1s">
                    <div class="row m0 featured-img">
                        <img src="<?php echo $base_url; ?>/images/ken.jpg" alt="">
                    </div>
                    <h4 class="title">Kensington Plaid</h4>



                </div>
                <!--==========Collection Items==========-->
                <div class="col-sm-6 col-md-3 item wow fadeIn" data-wow-delay="1.5s">
                    <div class="row m0 featured-img">
                        <img src="<?php echo $base_url; ?>/images/pink1.jpg" alt="">
                    </div>
                    <h4 class="title">Flamingo Bones</h4>



                </div>
            </div>
        </div>
    </section>

    <!--==========The Watch==========-->
    <section class="row the-watch">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-md-offset-1 text-center the-watch-img wow zoomIn">
                    <img src="<?php echo $base_url; ?>/images/pink.jpeg" alt="" class="img-responsive">
                </div>
                <div class="col-md-6 the-watch-features">
                    <div class="row section-header v3 wow fadeIn">
                        <h2>Not For Big Dogs</h2>

                        <p>Buttercup holds small dogs and cats close on all sides so they feel loved and safe through the night.</p><p>Large dogs will not fit inside the warm walls of a Buttercup. However, many large breed puppies will fit perfectly fine before their first growth spurt.</p>
                    </div>
                    <ul class="nav">
                        <li class="wow fadeIn" data-wow-delay="0.2s">Fits all pets under 10 pounds</li>
						<li class="wow fadeIn" data-wow-delay="0.4s">Sleeps nearly any puppy</li>
                        <li class="wow fadeIn" data-wow-delay="0.6s">Adult dogs from small breeds</li>
						<li class="wow fadeIn" data-wow-delay="0.8s">All house cats and breeds</li>
						<li class="wow fadeIn" data-wow-delay="1.0s">Large size available</li>
                    </ul>
                </div>
            </div>
        </div>
    </section>

    <!--==========Split Columns==========-->
    <section class="row split-columns">
        <div class="row m0 split-column wow fadeIn">
            <div class="col-sm-6 image text-right">
                <img src="<?php echo $base_url; ?>/images/upright.jpg" alt="">
            </div>
            <div class="col-sm-6 texts">
                <div class="texts-inner row m0">
                    <h2>One String Transforms<br/>a Warm & Cozy Hideout ...</h2>
                    <p>Pull nice and tight on the drawstring to bring your pet's Buttercup walls up high. Dogs and cats love to snuggle deep into their "nest" of a bed. Plush walls and a warm embrace give your pet the sensation of being burrowed safely alongside their mother.</p>
                </div>
            </div>
        </div>
        <div class="row m0 split-column wow fadeIn">
            <div class="col-sm-6 col-sm-push-6 image">
                <img src="<?php echo $base_url; ?>/images/flat.jpg" alt="">
            </div>
            <div class="col-sm-6 col-sm-pull-6 texts">
                <div class="texts-inner row m0">
                    <h2>... into an Inviting & Relaxing Hangout!</h2>
                    <p>Let loose on the drawstring to drop your pet's walls and give them a high-energy pad to romp around and play on. Buttercup's open pad works as a happy resting spot between rounds of fetch on a warm Sunday afternoon.</p>
                </div>
            </div>
        </div>
    </section>

    <!--==========Reviews==========-->
    <section class="row reviews" id="reviews">
        <div class="container">
            <div class="row section-header wow fadeInUp">
                <h2>Real Reviews from Real People</h2>
                <p>See what dog lovers like you are saying about the Buttercup Bed.<br/>These dogs love sleeping in their new bed!</p>
            </div>
            <div class="row">
                 <!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>My 10lb chi-weenie loves this. Curls up into a little ball and sleeps all night!!!</p>
					<p><b>- Samantha McCoyon</b></p>

                </div>
                <!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn" data-wow-delay="0.3s">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>Love this little buttercup, my dog Dantin Yea' jumped right in as soon as I opened the box. Wondeerful! So darn cute for words.</p>
					<p><b>- BLUESPACEFROGS</b></p>

                </div>
                <!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn" data-wow-delay="0.6s">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>The dog bed is great and like it very much. I have a little Shorkie and this fits her little body. I leave open enough where she can play with her toys in there.</p>
                   <p><b>- Linda from Alabama</b></p>
                </div>
            </div> <br/><br/>
			<div class="row">
			<!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>These are the best beds for small dogs. I have Shih Tzu and they all love it! Mine is microfiber and SO SOFT...it is adjustable so it can be real cozy or be made fuller in case two want to lay together..great bed!</p>
					<p><b>- A. Bellon</b></p>
                </div>
                <!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn" data-wow-delay="0.3s">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>Saw these buttercup beds and thought they were so cute and functional. Got the small for my cat. Lots of color combos so I could choose the one for my LR. Looks great, easy care, very good quality and my cat thinks it's heaven. Pros: Cute, unique, many color combos, very good quality.  Cons: Can't think of a single one.</p>
					<p><b>- Sara from Idaho</b></p>
                </div>
                <!--==========Review==========-->
                <div class="review col-sm-4 wow fadeIn" data-wow-delay="0.6s">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
					<p>My little dog loves this bed. I originally purchased one like this to fit inside a round fold-up bed. The bed finally broke, but the Buttercup dog bed continued to be used up until this year. I replaced it with this new one. The old bed held up well to numerous washings and drying, I expect the new one to do likewise. My dog is 14 pounds, so she uses this in the flat position. I have two, one for upstairs and one for downstairs. It's her favorite "spot."</p>
                    <p><b>- JWBon</b></p>
                </div>
               <div class="row">
			<!--==========Review==========-->
                <div class="review wow fadeIn">
                    <img src="<?php echo $base_url; ?>/images/quote.png" alt="" class="review-sign">
                    <p>I ordered 2 of these Beds as they are so stinking cute! My dogs loved them when I pulled them out of the box!!! LOVED LOVED LOVED the fabric! Very thick & sturdy & the color was SOO pretty! I would e ordered 2 more as I have 4 dogs, Boxers! Sadly, I had to return them :-( ONLY because they were too small for my breed of dogs. If they were still puppies, it would've never crossed my mind to send back! I almost kept them for when I do have future puppies... But since mine are only 2 yrs old, it just didn't make sense to keep them for 11-13 yrs. anyway... If you're looking for a very cute, fun, very cuddly bed for your little pooch, I HIGHLY recommend this Bed! 10 out of 10! And the sides are adjustable with a drawstring, which is handy! You can make it a flat round pad or draw it up like a Buttercup, which makes it nice & cozy!!!</p>
					<p><b>- Angie</b></p>
					</div>
                </div>
            </div>
        </div>
    </section>



    <!--==========FAQS==========-->
    <section class="row faqs" id="faq">
        <div class="container">
            <div class="row section-header wow fadeInUp">
                <h2>Frequent Questions</h2>
                <p>Got questions? We’ve got answers. If you have some other questions, feel free to send us an email to <a href="mailto:info@barkandwhiskers.com">info@barkandwhiskers.com</a></p>
            </div>
            <div class="row">
                <!--==========Faq==========-->
                <div class="col-sm-6 faq wow fadeInUp">
                    <h4>How is Buttercup different from other beds?</h4>
                    <p>Buttercup acts as two beds in one. With a tight string, it acts as a cozy nest for your dog to make his or her home. When the string is released, the Buttercup bed flattens and becomes a perfect pad for play and lounging. It can transform to fit your dog's mood at any moment. </p>
                </div>
                <!--==========Faq==========-->
                <div class="col-sm-6 faq wow fadeInUp">
                    <h4>What kind of fabric do you use?</h4>
                    <p>All Buttercup Beds come standard with a microvelvet fabric. This type of fabric is perfect at keeping your dog cool without any risk of overheating. It's easy to keep your pet at just the right temperature because microvelvet is so breathable.</p>
                </div>
                <!--==========Faq==========-->
                <div class="col-sm-6 faq wow fadeInUp" data-wow-delay="0.3s">
                    <h4>Is Buttercup washable?</h4>
                    <p>Yes! You can easily hand or machine wash your Buttercup. Just be sure to follow the simple instructions that come with your bed! It's easy to do. Check out the review section for buyer's opinions on keeping the Buttercup clean month after month.</p>
                </div>
                <!--==========Faq==========-->
                <div class="col-sm-6 faq wow fadeInUp" data-wow-delay="0.3s">
                    <h4>What sizes are available?</h4>
                    <p>We carry two sizes: small and large. The small bed measures 24"x1" and our large is 32"x1". We recommend purchasing the large if you're at all worried about your dog fitting into a Buttercup, .</p>
                </div>
            </div>
        </div>
    </section>



    <!--==========Mobile App==========-->
    <section class="row mobile-app">
        <div class="container">
            <div class="row">
                <div class="col-sm-6 col-sm-push-6 wow fadeIn">
                    <h2>Your Dog Will<br/>Feel Your Love</h2>
                    <p>Give your dog the gift of a warm embrace anytime you're away. Buttercup will become your dog's favorite place to rest, nap, and safely chew their favorite toys. Your dog will have a special place all their own, and he or she will be reminded of your love each and every morning.</p>
                    <div class="row m0 downloads-btns">
                       <a href="https://www.barkandwhiskers.com/product/bowsers-plush-buttercup/" target="_blank" class="btn btn-indegogo btn-lg">Choose My Buttercup Design</a>
                    </div>
                </div>
                <div class="col-sm-6 col-sm-pull-6 wow fadeInUp">
                    <img src="<?php echo $base_url; ?>/images/main2.1.png" alt="" class="mobile-img"><br/><br/><br/>
                </div>
            </div>
        </div>
    </section>





    <!--==========Footer==========-->
    <footer class="row">
        <div class="container">

            <div class="row m0 menu-rights">

                <p>Copyright © 2016. Bark & Whiskers.
                    <br class="small-divide"> All rights reserved</p>
            </div>
        </div>
    </footer>

	 <!-- ============================================================
                            Exit Intent Modal
         ============================================================ -->

    <!--  This will Boost up your Conversion rate -->
    <!-- Add  data-exit-modal="true" to the <body> to activate it -->

    <div id="exit-modal">
        <div class="underlay"></div>
        <div class="exit-modal">
            <div class="modal-title">
                <h3>Will a coupon change your mind?</h3>
            </div>

            <div class="modal-body">
                <p>Before you go, we want to give you one final reason to buy your Buttercup today.   If you sign up for our store's free newsletter you will instantly recieve a "10% Off Coupon" on your entire first purchase.  We promise to never spam you ... unless you think adorable puppies are spam.</p>

                <p class="text-center popup-cta"> <a href="https://www.barkandwhiskers.com/product/bowsers-plush-buttercup/" class=" btn btn-warning">Get Your Coupon (10% off)</a></p>

            </div>

            <div class="modal-footer">
                <p>No thanks</p>
            </div>
        </div>
    </div>

    <!-- // End Exit Intent Modal -->


    <!--========== Javascript Files ==========-->

    <!-- jQuery Latest -->
    <script src="<?php echo $base_url; ?>/js/lib/jquery-2.2.0.min.js"></script>

    <!-- Bootstrap JS -->
    <script src="<?php echo $base_url; ?>/js/lib/bootstrap.min.js"></script>

    <!-- Plugins -->
    <script src="<?php echo $base_url; ?>/vendors/owl-carousel/owl.carousel.js"></script>
    <script src="<?php echo $base_url; ?>/vendors/magnific-popup/jquery.magnific-popup.min.js"></script>
    <script src="<?php echo $base_url; ?>/vendors/bootstrap-touchspin/jquery.bootstrap-touchspin.min.js"></script>
    <script src="<?php echo $base_url; ?>/vendors/bootstrap-select/js/bootstrap-select.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js"></script>
    <script src="<?php echo $base_url; ?>/js/plugins/gmaps.min.js"></script>
    <script src="<?php echo $base_url; ?>/js/plugins/google-map.js"></script>
    <script src="<?php echo $base_url; ?>/js/plugins/wow.min.js"></script>
    <script src="<?php echo $base_url; ?>/js/plugins/validate.js"></script>
	  <script src="<?php echo $base_url; ?>/js/plugins/exitpopup.js"></script>
    <!-- Includes -->
    <script src="<?php echo $base_url; ?>/js/includes/pre-order.js"></script>
    <script src="<?php echo $base_url; ?>/js/includes/subscribe.js"></script>
    <script src="<?php echo $base_url; ?>/js/includes/contact.js"></script>

    <!-- Main JS -->
    <script src="<?php echo $base_url; ?>/js/main.js"></script>
    <script>
      (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
      (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
      m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
      })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');

      ga('create', 'UA-78202718-1', 'auto');
      ga('send', 'pageview');

    </script>
</body>

</html>
